import config as cfg
import requests
import sys
from modules.details import Details
from modules.overview import Overview
from modules.meters import Meters
from modules.production import Production
from datetime import datetime
from urllib.parse import urljoin

sys.path.insert(0, 'modules')

def main():
    details_api = Details(cfg.site_token)
    overview_api = Overview(cfg.site_token)
    meters_api = Meters(cfg.site_token, cfg.site_id)
    production_api = Production(cfg.site_token)

    while True:
        message = "Enter a command ('details', 'overview', 'meters', 'production', 'exit'):"
        command = input(message)
        if command == 'details':
            details = details_api.get_details(cfg.site_id)
            details_api.print_site_details(details)
        elif command == 'overview':
            try:
                overview_data = overview_api.get_site_overview(cfg.site_id)
                overview_api.print_site_overview(overview_data)
            except requests.exceptions.HTTPError as e:
                print(f"Error: {e}")
        elif command == 'meters':
            try:
                meters_data = meters_api.get_meter_data()
                meters_api.print_meter_data(meters_data)
            except requests.exceptions.HTTPError as e:
                print(f"Error: {e}")
        elif command == 'production':
            try:
                start_date = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
                end_date = datetime.now().replace(hour=23, minute=59, second=59, microsecond=999)
                production_data = production_api.get_daily_production(start_date, end_date)
                production_api.print_daily_production(production_data)
            except requests.exceptions.HTTPError as e:
                print(f"Error: {e}")
        elif command == 'exit':
            break
        else:
            print("Invalid command. Please try again.")

if __name__ == "__main__":
    main()
