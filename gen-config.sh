#!/bin/bash

BASEURL="https://monitoringapi.solaredge.com"
CONFIG_FILE="config.py"
echo -n "Enter your Solaredge's token: "
read -r site_token
echo -n "Enter your Solaredge's site id: "
read -r site_id
echo "BASEURL = '$BASEURL'" > $CONFIG_FILE
echo "site_token = '$site_token'" >> $CONFIG_FILE
echo "site_id = '$site_id'" >> $CONFIG_FILE
echo "Configuration written to $CONFIG_FILE"
