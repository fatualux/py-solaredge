import requests
from urllib.parse import urljoin
import config as cfg

class Production:
    def __init__(self, token):
        self.token = token

    def get_daily_production(self, start_date, end_date):
        """
        Get daily production data for the specified time period.
        """
        url = urljoin(cfg.BASEURL, f"site/{cfg.site_id}/energy")
        params = {
            'api_key': cfg.site_token,
            'timeUnit': 'DAY',
            'startDate': start_date.strftime('%Y-%m-%d'),
            'endDate': end_date.strftime('%Y-%m-%d')
        }

        print("Request URL:", url)  # Print the request URL with parameters
        print("Request Params:", params)  # Print the request parameters
        r = requests.get(url, params=params)
        print("Response:", r.content)  # Print the response content
        r.raise_for_status()
        return r.json()

    def print_daily_production(self, daily_production):
        """
        Print daily production data.
        """
        print("Daily Production:")
        unit = daily_production['energy'].get('unit')
        for day in daily_production['energy']['values']:
            print(f"Date: {day['date']}, Production: {day['value']} {unit}")
