import requests
from urllib.parse import urljoin
import config as cfg


class Overview:
    def __init__(self, token):
        self.token = token

    def get_site_overview(self, site_id):
        """
        Get site overview data.
        """
        url = urljoin(cfg.BASEURL, f"site/{site_id}/overview")
        params = {
            'api_key': cfg.site_token
        }

        print("Request URL:", url)
        print("Request Params:", params)
        r = requests.get(url, params=params)
        print("Response:", r.content)
        r.raise_for_status()
        return r.json()

    def print_site_overview(self, overview_data):  # Added self parameter here
        overview = overview_data.get('overview')
        if overview:
            print("Site Overview:")
            print(f"Last Update Time: {overview.get('lastUpdateTime')}")
            print("Life Time Data:")
            print(f"  Energy: {overview['lifeTimeData']['energy']}")
            print(f"  Revenue: {overview['lifeTimeData']['revenue']}")
            print("Last Year Data:")
            last_year_data = overview.get('lastYearData')
            if last_year_data:
                print(f"  Energy: {last_year_data.get('energy')}")
                print(f"  Revenue: {last_year_data.get('revenue')}")
            else:
                print("  No data available for last year.")
            print("Last Month Data:")
            last_month_data = overview.get('lastMonthData')
            if last_month_data:
                print(f"  Energy: {last_month_data.get('energy')}")
                print(f"  Revenue: {last_month_data.get('revenue')}")
            else:
                print("  No data available for last month.")
            print("Last Day Data:")
            last_day_data = overview.get('lastDayData')
            if last_day_data:
                print(f"  Energy: {last_day_data.get('energy')}")
                print(f"  Revenue: {last_day_data.get('revenue')}")
            else:
                print("  No data available for last day.")
            print("Current Power:")
            print(f"  Power: {overview['currentPower']['power']}")
        else:
            print("No site overview data found.")
