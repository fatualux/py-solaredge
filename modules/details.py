import requests
from urllib.parse import urljoin
import config as cfg


class Details:
    def __init__(self, token):
        self.token = token

    def get_details(self, site_id):
        url = urljoin(cfg.BASEURL, f"site/{site_id}/details")
        url += f"?api_key={self.token}"

        print("Request URL:", url)
        r = requests.get(url)
        r.raise_for_status()

        try:
            site_details = r.json()
        except ValueError:
            raise ValueError("Failed to parse response as JSON")

        return site_details

    def print_site_details(self, details):  # Added self parameter here
        details = details['details']
        print("Site Details:")
        print(f"id: {details['id']}")
        print(f"name: {details['name']}")
        print(f"accountId: {details['accountId']}")
        print(f"status: {details['status']}")
        print(f"peakPower: {details['peakPower']}")
        print(f"lastUpdateTime: {details['lastUpdateTime']}")
        print(f"installationDate: {details['installationDate']}")
        print(f"ptoDate: {details['ptoDate']}")
        print()
        print("Location:")
        location = details['location']
        print(f"country: {location['country']}")
        print(f"city: {location['city']}")
        print(f"address: {location['address']}")
        print()
        print("PrimaryModule:")
        primary_module = details['primaryModule']
        print(f"manufacturerName: {primary_module['manufacturerName']}")
        print(f"modelName: {primary_module['modelName']}")
        print(f"maximumPower: {primary_module['maximumPower']}")
        print(f"temperatureCoef: {primary_module['temperatureCoef']}")
        print(f"{urljoin(cfg.BASEURL, '/site/' + str(details['id']))}")
